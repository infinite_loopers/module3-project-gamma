import MovieCard from "./MovieCard";
import { useGetMovieByIdQuery } from "./app/apiSlice";

const FavoriteCard = ({ favorite }) => {
    const { data, isLoading } = useGetMovieByIdQuery(favorite.movie_id);

    if (isLoading) return <div>Loading...</div>;

    return <MovieCard movie={data.movie} />;
}

export default FavoriteCard;
