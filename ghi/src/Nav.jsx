import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { useGetAccountQuery, useLogoutMutation } from "./app/apiSlice";
import { useNavigate } from "react-router-dom";
import logoIcon from './logoIcon.png';


const Nav = () => {
  const [logoutSuccess, setLogoutSuccess] = useState(false);
  const { data: account } = useGetAccountQuery();
  const [logout] = useLogoutMutation();

  const navigate = useNavigate();


  useEffect(() => {
    setLogoutSuccess(false);
  }, [account]);

  const handleLogout = async () => {
    await logout();
    setLogoutSuccess(true);
    navigate("/");
  };

  if (logoutSuccess) {
    return null;
  }

  return (
    <nav className="navbar navbar-expand-md bg-body-tertiary sticky-nav">
      <div className="container-fluid">
        <NavLink to={"/"} className="navbar-brand">
          <img src={logoIcon} alt="Logo" width="60" height="55" style={{ paddingRight: "5px" }} />
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink to={"/home"} className="nav-link">
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={"/search"} className="nav-link">
                Search
              </NavLink>
            </li>
            {account && (
              <li className="nav-item">
                <NavLink to={"/bingelist"} className="nav-link">
                  Binge List
                </NavLink>
              </li>
            )}
          </ul>
          <ul className="navbar-nav ml-auto mb-2 mb-lg-0">
            {!account && (
              <>
                <li className="nav-item">
                  <NavLink to={"/login"} className="nav-link auth-link">
                    Login
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to={"/signup"} className="nav-link auth-link">
                    Sign Up
                  </NavLink>
                </li>
              </>
            )}
            {account && (
              <li className="nav-item">
                <button
                  className="nav-link btn btn-link auth-link"
                  onClick={handleLogout}
                >
                  Logout
                </button>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
