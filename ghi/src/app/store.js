import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/dist/query";

import { movieApi } from "./apiSlice";
import movieSearchReducer from "./movieSearchSlice";

export const store = configureStore({
    reducer: {
        movieSearch: movieSearchReducer,
        [movieApi.reducerPath]: movieApi.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(movieApi.middleware),
})
setupListeners(store.dispatch)
