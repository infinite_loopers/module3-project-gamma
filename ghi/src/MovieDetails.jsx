import { useParams } from "react-router-dom";
import { useGetMovieByIdQuery, useGetMovieVideoQuery } from "./app/apiSlice";
import FavoriteButton from "./FavoritesButton";

const MovieDetails = () => {
    const { movie_id } = useParams();
    const { data, isLoading } = useGetMovieByIdQuery(movie_id);
    const { data: videos, isLoading: videosLoading } = useGetMovieVideoQuery(movie_id);
    const posterUrl = "https://image.tmdb.org/t/p/original";
    const videoUrl = "https://www.youtube.com/embed/";

    if (isLoading || videosLoading) return <div>Loading...</div>;
    return (
        <div className="container vh-105" style={{ marginTop: "160px" }}>
            <div className="row align-items-start">
                <div className="col-md-6 vh-100 text-center" style={{ marginTop: "-25px", marginBottom: "-10px" }}>
                    <h4 className="card-title" style={{ color: '#00c7d1', fontSize: "35px", whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis", maxWidth: "80%", margin: "0 auto", paddingBottom: "10px" }}>
                        {data.movie.original_title}
                    </h4>
                    {data.movie.poster_path && (
                        <img
                            src={`${posterUrl}${data.movie.poster_path}`}
                            alt={`Poster for ${data.movie.original_title}`}
                            className="card-img-top"
                            style={{ display: "block", margin: "0 auto", maxWidth: '500px' }}
                        />
                    )}
                </div>
                <div className="col-md-6 vh-80">
                    <div className="card">
                        <div className="card-body">
                            <div style={{ paddingTop: "25px", paddingBottom: '0px' }}>
                                <FavoriteButton movieId={movie_id} />
                            </div>
                            <h5 className="card-title" style={{ fontSize: '2em', paddingTop: '30px', color: '#09f0a0' }}>Overview</h5>
                            <p className="card-text" style={{ fontSize: '1.5em' }}>{data.movie.overview}</p>
                            <p className="card-text" style={{ color: "#00c7d1" }}>
                                Vote average: {data.movie.vote_average}
                            </p>
                            <p>Genres: {data.movie.genres.map((genre) => genre.name).join(", ")}</p>
                        </div>
                        <div className="card-footer text-left">
                            <h5 style={{ fontSize: '2em', color: '#09f0a0', paddingBottom: "10px" }}>Trailer</h5>
                            {videos.movie.type ? (
                                <div style={{ textAlign: 'left' }}>
                                    <iframe
                                        title="Movie Trailer"
                                        width="560"
                                        height="315"
                                        src={`${videoUrl}${videos.movie.key}`}
                                        frameBorder="0"
                                        allowFullScreen
                                    ></iframe>
                                </div>
                            ) : <p>{videos.movie.name}</p>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MovieDetails;
