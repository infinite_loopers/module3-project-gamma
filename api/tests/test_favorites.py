from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoritesQueries
from authenticator import authenticator

client = TestClient(app)


class FakeFavoritesQueries:
    def get_all(self, user_id):
        return [{"id": 0, "movie_id": "550", "user_id": user_id}]

    def create(self, favorite_in, user_id):
        return {"movie_id": favorite_in.movie_id, "user_id": user_id}


class FakeAuthenticator:
    def get_current_account_data(self):
        return {"id": "test_id", "email": "test@test.com"}


def test_get_all_favorites():
    # Arrange
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = FakeAuthenticator().get_current_account_data

    # Act
    res = client.get("/api/favorites/")
    data = res.json()

    # Assert
    assert res.status_code == 200
    assert len(data) == 1
    assert data[0]["user_id"] == "test_id"
    assert data[0]["movie_id"] == "550"

    # Cleanup
    app.dependency_overrides = {}


def test_add_favorite():
    app.dependency_overrides[FavoritesQueries] = FakeFavoritesQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = FakeAuthenticator().get_current_account_data

    # Act
    response = client.post("/api/favorites/", json={"movie_id": "550"})
    data = response.json()

    # Assert
    assert response.status_code == 200
    assert data["user_id"] == "test_id"
    assert data["movie_id"] == "550"

    # cleanup
    app.dependency_overrides = {}
