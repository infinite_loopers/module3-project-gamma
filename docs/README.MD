# Project Documentation

##  Infinite Binge Team

Chad North
Liz Ward
Sam Dorismond
Kaysi McGinty
Humza Qureshi


## Infinite Binge
Discover, Track, and Binge: Your Ultimate Movie Companion


## Design

- [API design](docs/apis/) - view screenshots of API routes through FastAPI, including Accounts, Movies, and Favorites (a.k.a. "Binge")

- [GHI wireframes](docs/wireframes/)

- [Integrations](docs/integrations.md)


## Intended market

We are targeting movie enthusiasts and passionate film lovers seeking a personalized movie experience. From casual viewers to cinephiles, Infinite Binge serves the needs of diverse moviegoers, providing a one-stop destination for their movie-related interests.



## Functionality:

* User Registration: Visitors can create an account on Binge List to access personalized features and preferences.

* Movie Categories: Users can browse and view movies categorized as top rated, upcoming, and now playing.

* Movie Search: Users can search for movies by entering keywords or specific titles to find relevant results.

* Personalized Binge List: Users can create a personalized binge list by adding movies of interest. They can also remove movies from their binge list as per their preferences.

* Movie Details: Clicking on a movie allows users to view detailed information about the movie, including its synopsis and rating.


## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
1. CD into the new project directory
1. Run `docker volume create mongo-data`
1. Run `docker compose build`
1. Run `docker compose up`
1. Follow the directions in [integrations](docs/integrations.md) to add an API key to enable access to The Movie Database.
1. Open a browser and navigate to http://localhost:3000/
1. Enjoy binging your movies!
